package company.com.stepikapi.view.ui

import android.content.Context

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import company.com.stepikapi.R
import company.com.stepikapi.model.database.AppDatabase
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.viewmodel.adapter.CourseAdapter
import java.io.Serializable

private const val ARG_COLUMN_COUNT = "1"
private const val LIST = "list"

class ItemFragment : Fragment() {
    private var mListener: OnListFragmentInteractionListener? = null
    private var mColumnCount = 1
    private var courseAdapter: CourseAdapter? = null
    private var courses: MutableList<Course>? = null

    companion object {

        fun newInstance(columnCount: Int, courses: List<Course>): ItemFragment {
            val fragment = ItemFragment()
            val args = Bundle()
            // send list of courses to fragment
            args.putSerializable(LIST, courses as Serializable)
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        retainInstance = true
        Log.v("item", "started")

        // check extra
        arguments?.let {
            mColumnCount = it.getInt(ARG_COLUMN_COUNT)
            courses = it.getParcelableArrayList(LIST)
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        if (view is RecyclerView) {
            context?.let {
                courseAdapter = CourseAdapter(
                        AdapterClickListener(activity),
                        AppDatabase.getAppDatabase(it.applicationContext)
                )
            }

            val viewContext = view.getContext()

            if (mColumnCount <= 1) {
                view.layoutManager = LinearLayoutManager(viewContext)
            } else {
                view.layoutManager = GridLayoutManager(viewContext, mColumnCount)
            }

            view.addItemDecoration(
                    DividerItemDecoration(view.context, DividerItemDecoration.VERTICAL)
            )

            view.adapter = courseAdapter
            courses?.let {
                if (it.isNotEmpty()) {
                    courseAdapter?.data = it
                }
            }

        }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    private data class AdapterClickListener(
            val activity: FragmentActivity?
    ) : CourseAdapter.CourseViewHolder.OnItemClickListener {
        override fun onClick(course: Course) {
            activity?.let {
                DetailsActivity.start(it, course)
            }
        }

    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Int)

    }

}
