package company.com.stepikapi.model.api

import android.app.Application
import android.content.Context
import com.google.gson.GsonBuilder
import company.com.stepikapi.BuildConfig
import company.com.stepikapi.model.entity.Search
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

class AppDelegate : Application() {

    var apiService: Api? = null
        private set

    companion object {
        fun from(context: Context): AppDelegate {
            return context.applicationContext as AppDelegate
        }

    }

    override fun onCreate() {
        super.onCreate()
        initRetrofit()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initRetrofit() {
        val json = GsonBuilder().also {
            it.registerTypeAdapter(Search::class.java, SearchDeserializer())
        }.create()

        apiService = Retrofit.Builder()
                .baseUrl("https://stepik.org/api/")
                .addConverterFactory(GsonConverterFactory.create(json))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api::class.java)
    }

}

