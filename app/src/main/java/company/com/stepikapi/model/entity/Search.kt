package company.com.stepikapi.model.entity

class Search {
    private val courseList: MutableList<Course> = mutableListOf()

    fun add(course: Course) {
        courseList.add(course)
    }

    fun getCourseList(): List<Course> {
        return courseList
    }

}
