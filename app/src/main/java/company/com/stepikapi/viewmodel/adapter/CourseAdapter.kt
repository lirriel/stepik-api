package company.com.stepikapi.viewmodel.adapter

import android.content.Context

import android.graphics.drawable.Drawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import company.com.stepikapi.R
import company.com.stepikapi.model.database.AppDatabase
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.view.utils.BitmapTranslator

/**
 * Adapter class for recyclerView presenting courses find in search
 */
class CourseAdapter(
        private val listener: CourseViewHolder.OnItemClickListener,
        appDatabase: AppDatabase
) : RecyclerView.Adapter<CourseAdapter.CourseViewHolder>() {

    private var items: MutableList<Course> = mutableListOf()

    var data: MutableList<Course>
        get() = items
        set(newItems) {
            items = newItems
            notifyDataSetChanged()
        }

    companion object {
        private var appDatabase: AppDatabase? = null
    }

    init {
        CourseAdapter.appDatabase = appDatabase
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseViewHolder {
        val layout = LayoutInflater.from(parent.context)
                .inflate(R.layout.item, parent, false)
        return CourseViewHolder(layout, listener)
    }

    override fun onBindViewHolder(holder: CourseViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun add(item: Course) {
        items.add(item)
        notifyItemInserted(items.size - 1)
    }

    class CourseViewHolder(
            itemView: View,
            var listener: OnItemClickListener?
    ) : RecyclerView.ViewHolder(itemView) {
        var context: Context = itemView.context
        var courseName: TextView = itemView.findViewById(R.id.title)
        var courseLogo: ImageView = itemView.findViewById(R.id.cover)

        var favourite: Button = itemView.findViewById(R.id.button_favourite)

        /**
         * Save favourite courses to database
         */
        fun setFave(isFavourite: Boolean) {
            if (isFavourite)
                favourite.background = ContextCompat.getDrawable(context, R.drawable.star_on)
            else
                favourite.background = ContextCompat.getDrawable(context, R.drawable.star_off)
        }

        fun bind(courseInfo: Course) {
            Log.v("bind", "started")
            appDatabase?.courseDao?.isIn(courseInfo.courseTitle.toString())?.let {
                courseInfo.isFave = true
                favourite.background = ContextCompat.getDrawable(context, R.drawable.star_on)
            }

            if (listener != null) {
                itemView.setOnClickListener { listener?.onClick(courseInfo) }
            }
            courseName.text = courseInfo.courseTitle

            favourite.setOnClickListener {
                if (courseInfo.isFave) {
                    courseInfo.isFave = false
                    appDatabase?.courseDao?.delete(courseInfo)
                } else {
                    courseInfo.isFave = true
                    appDatabase?.courseDao?.insert(courseInfo)
                }
                setFave(courseInfo.isFave)
                Log.v("fave", "added")
            }

            setFave(courseInfo.isFave)

            val options = RequestOptions()
                    .centerCrop()
                    .placeholder(R.mipmap.ic_launcher_round)
                    .fallback(android.R.color.holo_orange_light)
                    .error(R.mipmap.ic_launcher_round)

            val image = appDatabase?.courseDao?.getCover(courseInfo.id)
            if (image != null) {
                courseLogo.setImageDrawable(BitmapTranslator.bitmapToDrawable(BitmapTranslator.getImage(image), context))
                Log.v("bind", "load from db")
            } else {
                if (courseInfo.cover != null) {
                    courseLogo.setImageDrawable(
                            BitmapTranslator.bitmapToDrawable(
                                    BitmapTranslator.getImage(courseInfo.cover
                                            ?: throw NullPointerException("Cover is null")
                                    ),
                                    context
                            )
                    )
                } else {
                    courseInfo.urlCover?.let {
                        Glide.with(itemView)
                                .load(it)
                                .apply(options)
                                .listener(object : RequestListener<Drawable> {
                                    override fun onLoadFailed(
                                            e: GlideException?,
                                            model: Any,
                                            target: Target<Drawable>,
                                            isFirstResource: Boolean
                                    ): Boolean {
                                        return false
                                    }

                                    override fun onResourceReady(
                                            resource: Drawable,
                                            model: Any,
                                            target: Target<Drawable>,
                                            dataSource: DataSource,
                                            isFirstResource: Boolean
                                    ): Boolean {
                                        courseInfo.cover = BitmapTranslator.getBitmapAsByteArray(
                                                BitmapTranslator.drawableToBitmap(resource)
                                        )
                                        Log.v("bind", "load success")
                                        appDatabase?.courseDao?.updateCurrencyImage(
                                                courseInfo.id,
                                                courseInfo.cover
                                                        ?: throw NullPointerException("Cover is null")
                                        )
                                        return false
                                    }
                                }).into(courseLogo)
                    }
                }
            }
        }

        /**
         * Click listener for each item in interface
         */
        interface OnItemClickListener {
            fun onClick(course: Course)
        }

    }
}
