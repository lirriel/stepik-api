package company.com.stepikapi.view.ui

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import company.com.stepikapi.R
import company.com.stepikapi.model.api.AppDelegate
import company.com.stepikapi.model.database.AppDatabase
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.viewmodel.adapter.CourseAdapter

private const val ARG_COLUMN_COUNT = "1"

class FaveFragment : Fragment() {
    private var mColumnCount = 1
    private var mListener: OnListFragmentInteractionListener? = null
    private var courseAdapter: CourseAdapter? = null
    private var appDelegate: AppDelegate? = null
    private var courses: MutableList<Course>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        context?.let {
            appDelegate = AppDelegate.from(it)
        }
        retainInstance = true

        Log.v("item", "started")
        if (courses == null) {
            courses = context?.let {
                AppDatabase.getAppDatabase(it.applicationContext)
                        .courseDao
                        .allCourse
                        .toMutableList()
            }
        }
        arguments?.getInt(ARG_COLUMN_COUNT)?.let {
            mColumnCount = it
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        // Set the adapter
        if (view is RecyclerView) {
            // init course adapter
            context?.let {
                courseAdapter = CourseAdapter(
                        AdapterClickListener(activity),
                        AppDatabase.getAppDatabase(it.applicationContext)
                )
            }

            val context = view.getContext()

            if (mColumnCount <= 1) {
                view.layoutManager = LinearLayoutManager(context)
            } else {
                view.layoutManager = GridLayoutManager(context, mColumnCount)
            }

            // set adapter data
            view.adapter = courseAdapter
            courses?.let {
                if (it.isNotEmpty()) {
                    courseAdapter?.data = it
                }
            }
        }
        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnListFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException("$context must implement OnListFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListener = null
    }

    private data class AdapterClickListener(
            val activity: FragmentActivity?
    ) : CourseAdapter.CourseViewHolder.OnItemClickListener {
        override fun onClick(course: Course) {
            activity?.let {
                DetailsActivity.start(it, course)
            }
        }
    }

    interface OnListFragmentInteractionListener {
        fun onListFragmentInteraction(item: Int)
    }

    companion object {

        fun newInstance(columnCount: Int): ItemFragment {
            val fragment = ItemFragment()
            val args = Bundle()
            args.putInt(ARG_COLUMN_COUNT, columnCount)
            fragment.arguments = args
            return fragment
        }
    }
}
