package company.com.stepikapi.model.api

import company.com.stepikapi.model.entity.Search

data class ApiResponse(val search: Search? = null)