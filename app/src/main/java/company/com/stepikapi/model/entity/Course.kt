package company.com.stepikapi.model.entity

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class Course(
        @PrimaryKey(autoGenerate = true)
        val innerId: Long = 0,
        @SerializedName("id")
        @Expose
        @ColumnInfo
        var id: Long,
        @SerializedName("score")
        @Expose
        @ColumnInfo
        var score: Double = 0.toDouble(),
        @SerializedName("targetType")
        @Expose
        @ColumnInfo
        var targetType: String? = null,
        @SerializedName("course")
        @Expose
        @ColumnInfo
        var course: Int = 0,
        @SerializedName("courseOwner")
        @Expose
        @ColumnInfo
        var courseOwner: Long = 0,
        @SerializedName("courseTitle")
        @Expose
        @ColumnInfo
        var courseTitle: String? = null,
        @SerializedName("cover")
        @Expose
        @ColumnInfo
        var cover: ByteArray? = null,
        @SerializedName("urlCover")
        @Expose
        @ColumnInfo
        var urlCover: String? = null,
        @SerializedName("isFave")
        @Expose
        @ColumnInfo
        var isFave: Boolean = false
) : Parcelable, Serializable {
    constructor(parcel: Parcel) : this(
            parcel.readLong(),
            parcel.readLong(),
            parcel.readDouble(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readLong(),
            parcel.readString(),
            parcel.createByteArray(),
            parcel.readString(),
            parcel.readByte() != 0.toByte())


    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(innerId)
        parcel.writeLong(id)
        parcel.writeDouble(score)
        parcel.writeString(targetType)
        parcel.writeInt(course)
        parcel.writeLong(courseOwner)
        parcel.writeString(courseTitle)
//        parcel.writeByteArray(cover)
        parcel.writeString(urlCover)
        parcel.writeByte(if (isFave) 1 else 0)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Course> {
        override fun createFromParcel(parcel: Parcel): Course {
            return Course(parcel)
        }

        override fun newArray(size: Int): Array<Course?> {
            return arrayOfNulls(size)
        }
    }

}