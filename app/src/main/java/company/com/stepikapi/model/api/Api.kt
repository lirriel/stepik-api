package company.com.stepikapi.model.api

import company.com.stepikapi.model.entity.Search
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET("search-results")
    fun getSearch(@Query("query") course: String): Call<Search>
}