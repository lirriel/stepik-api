package company.com.stepikapi.view.ui

import android.app.Activity
import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.os.Parcelable
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import company.com.stepikapi.R
import company.com.stepikapi.model.api.AppDelegate
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.model.entity.Search
import company.com.stepikapi.view.utils.BitmapTranslator
import io.reactivex.annotations.NonNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

private const val COURSE_TAG = "course"

class DetailsActivity : AppCompatActivity() {
    private var title: TextView? = null
    private var author: TextView? = null
    private var type: TextView? = null
    private var imageView: ImageView? = null
    private var course: Course? = null
    private var appDelegate: AppDelegate? = null

    companion object {
        fun start(activity: Activity, course: Course) {
            val intent = Intent(activity, DetailsActivity::class.java)
            course.cover = null
            intent.putExtra(COURSE_TAG, course as Parcelable)
            activity.startActivity(intent)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        appDelegate = AppDelegate.from(applicationContext)
        setContentView(R.layout.activity_details)
    }

    override fun onResume() {
        super.onResume()
        if (course == null && intent != null) {
            course = intent.getParcelableExtra(COURSE_TAG)
        }

        course?.let {
            init()
            setCourse(it)
        }
    }

    /**
     * initialize view
     */
    private fun init() {
        author = findViewById(R.id.course_author)
        type = findViewById(R.id.course_type)
        title = findViewById(R.id.title)
        imageView = findViewById(R.id.course_logo)
    }

    /**
     * Set info about course on activity
     */
    private fun setCourse(course: Course) {
        type?.text = course.targetType
        title?.text = course.courseTitle
        author?.text = "${course.courseOwner}"
        getCourseCover(course)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(COURSE_TAG, course)
        Log.d(COURSE_TAG, "onSaveInstanceState")
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        course = savedInstanceState.getParcelable(COURSE_TAG)
        Log.d(COURSE_TAG, "onRestoreInstanceState")
    }

    private fun getCourseCover(course: Course) {
        val options = RequestOptions()
                .centerCrop()
                .placeholder(R.mipmap.ic_launcher_round)
                .fallback(android.R.color.holo_orange_light)
                .error(R.mipmap.ic_launcher_round)
        System.err.println("search " + course)

        appDelegate
                ?.apiService
                ?.getSearch(course.courseTitle.orEmpty())
                ?.enqueue(object : Callback<Search> {
                    override fun onResponse(
                            @NonNull call: Call<Search>,
                            @NonNull response: Response<Search>
                    ) {
                        System.err.println("list " + response.body()?.getCourseList())
                        findViewById<View>(R.id.error_layout).visibility = View.GONE
                        response.body()
                                ?.getCourseList()
                                ?.filter { it.courseTitle == course.courseTitle }
                                ?.get(0)
                                ?.urlCover
                                ?.let {
                                    Glide.with(imageView)
                                            .load(it)
                                            .apply(options)
                                            .listener(object : RequestListener<Drawable> {
                                                override fun onLoadFailed(
                                                        e: GlideException?,
                                                        model: Any,
                                                        target: Target<Drawable>,
                                                        isFirstResource: Boolean
                                                ): Boolean {
                                                    return false
                                                }

                                                override fun onResourceReady(
                                                        resource: Drawable,
                                                        model: Any,
                                                        target: Target<Drawable>,
                                                        dataSource: DataSource,
                                                        isFirstResource: Boolean
                                                ): Boolean {
                                                    course.cover = BitmapTranslator.getBitmapAsByteArray(
                                                            BitmapTranslator.drawableToBitmap(resource)
                                                    )

                                                    Log.v("bind cover", "load success")
                                                    return false
                                                }
                                            }).into(imageView)
                                }
                    }

                    override fun onFailure(call: Call<Search>, t: Throwable) {
                        Log.v("search - detailed", "failed to load")
                    }
                })
    }

}
