package company.com.stepikapi.view.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import com.miguelcatalan.materialsearchview.MaterialSearchView
import company.com.stepikapi.R
import company.com.stepikapi.model.api.AppDelegate
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.model.entity.Search
import io.reactivex.annotations.NonNull
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.Serializable

class StartActivity : AppCompatActivity(),
        NavigationView.OnNavigationItemSelectedListener,
        ItemFragment.OnListFragmentInteractionListener,
        FaveFragment.OnListFragmentInteractionListener {

    private var searchView: MaterialSearchView? = null
    private var appDelegate: AppDelegate? = null
    private var courses: List<Course>? = null
    private var fragment: Fragment? = null
    private var progressBar: ProgressBar? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        init()
        setListeners()
    }

    private fun displaySelectedScreen(itemId: Int) {
        when (itemId) {
            R.id.nav_search -> fragment = ItemFragment()
            R.id.nav_fave -> fragment = FaveFragment()
        }

        fragment?.let {
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.content_frame, it)
            ft.commit()
        }

        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
    }

    /**
     * Load search request from net using api
     * @param searchTxt
     */
    @SuppressLint("CheckResult")
    private fun loadSearch(searchTxt: String) {
        appDelegate
                ?.apiService
                ?.getSearch(searchTxt)
                ?.enqueue(object : Callback<Search> {
                    override fun onResponse(
                            @NonNull call: Call<Search>,
                            @NonNull response: Response<Search>
                    ) {
                        findViewById<View>(R.id.error_layout).visibility = View.GONE
                        progressBar?.visibility = View.GONE
                        courses = response.body()?.getCourseList()

                        if (courses.isNullOrEmpty()) {
                            findViewById<View>(R.id.error_layout).visibility = View.VISIBLE
                            fragment?.view?.visibility = View.GONE
                        } else {
                            courses?.let {
                                fragment = ItemFragment.newInstance(1, it)
                            }
                            fragment?.arguments = Bundle().also {
                                it.putSerializable("list", courses as Serializable?)
                            }
                            val ft = supportFragmentManager.beginTransaction()
                            fragment?.let {
                                ft.replace(R.id.content_frame, it)
                                ft.commit()
                                Log.v("transaction", "commit")
                            }
                        }
                    }

                    override fun onFailure(call: Call<Search>, t: Throwable) {
                        Log.v("search", "failed to load")
                        Log.e("search", t.message, t)
                        Log.i("search", call.request().toString())
                        progressBar?.visibility = View.GONE
                        findViewById<View>(R.id.error_layout).visibility = View.VISIBLE

                        fragment?.let {
                            it.view?.visibility = View.GONE
                        }
                    }
                })
    }

    private fun setListeners() {
        searchView?.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                progressBar?.visibility = View.VISIBLE
                loadSearch(query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                progressBar?.visibility = View.VISIBLE
                return false
            }
        })

        searchView?.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() {
                searchView?.visibility = View.VISIBLE
            }

            override fun onSearchViewClosed() {
                searchView?.visibility = View.GONE
            }
        })
    }

    /**
     * Initialize activity with variables
     */
    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
                this,
                drawer,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
        searchView = findViewById(R.id.search_view)
        appDelegate = AppDelegate.from(applicationContext)
        progressBar = findViewById(R.id.progress_bar)
        progressBar?.visibility = View.GONE
    }

    override fun onBackPressed() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.start, menu)
        searchView?.setMenuItem(menu.findItem(R.id.action_search))
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return if (item.itemId == R.id.action_search) {
            true
        } else {
            super.onOptionsItemSelected(item)
        }

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        displaySelectedScreen(item.itemId)
        return true
    }

    override fun onListFragmentInteraction(item: Int) {
        Log.v("item", "click")
    }
}
