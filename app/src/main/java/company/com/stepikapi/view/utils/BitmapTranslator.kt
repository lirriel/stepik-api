package company.com.stepikapi.view.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.ImageDecoder
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable

import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer

object BitmapTranslator {
    /**
     * Translate Bitmap to byte array to save to Room database
     */
    fun getBitmapAsByteArray(bitmap: Bitmap): ByteArray {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, outputStream)
        return outputStream.toByteArray()
    }

    /**
     * Translate byte[] array from database to Bitmap
     */
    fun getImage(imgByte: ByteArray): Bitmap {
        return ImageDecoder.decodeBitmap(ImageDecoder.createSource(ByteBuffer.wrap(imgByte)))
//        return BitmapFactory.decodeByteArray(imgByte, 0, imgByte.size)
    }

    /**
     * Change bitmap to drawable
     */
    fun bitmapToDrawable(bitmap: Bitmap, context: Context): Drawable {
        return BitmapDrawable(context.resources, bitmap)
    }

    /**
     * Change drawable to bitmap
     */
    fun drawableToBitmap(drawable: Drawable): Bitmap {

        if (drawable is BitmapDrawable) {
            if (drawable.bitmap != null) {
                return drawable.bitmap
            }
        }

        val bitmap: Bitmap = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
            Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888)
        } else {
            Bitmap.createBitmap(drawable.intrinsicWidth, drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888)
        }

        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)
        return bitmap
    }
}
