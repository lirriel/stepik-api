package company.com.stepikapi.model.api

import android.util.Log
import com.google.gson.*
import company.com.stepikapi.model.entity.Course
import company.com.stepikapi.model.entity.Search
import java.lang.reflect.Type

/**
 * Deserializer for search = for api response
 */
class SearchDeserializer : JsonDeserializer<Search> {

    @Throws(JsonParseException::class)
    override fun deserialize(
            json: JsonElement,
            typeOfT: Type,
            context: JsonDeserializationContext
    ): Search {
        val search = Search()

        if (json.isJsonObject) {
            json.asJsonObject.getAsJsonArray("search-results").forEach {
                with(it.asJsonObject) {
                    try {
                        Course(
                                course = get("course").asInt,
                                courseOwner = get("course").asLong,
                                id = get("id").asLong,
                                courseTitle = getOrNull("course_title"),
                                score = get("score").asDouble,
                                targetType = get("target_type").asString,
                                urlCover = getOrNull("course_cover")
                        ).also { course -> search.add(course) }
                    } catch (e: Exception) {
                        Log.e("deserializeError", e.message)
                    }
                }
            }
        }

        return search
    }

}

private fun JsonObject.getOrNull(name: String): String? {
    return try {
        get(name).asString
    } catch (e: Exception) {
        null
    }
}
