package company.com.stepikapi.model.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query

import company.com.stepikapi.model.entity.Course

@Dao
interface CourseDao {

    @get:Query("SELECT * FROM course")
    val allCourse: List<Course>

    @Insert
    fun insertAll(vararg courses: Course)

    @Query("DELETE FROM course")
    fun deleteAll()

    @Delete
    fun delete(course: Course)

    @Insert
    fun insert(course: Course)

    @Query("SELECT COUNT(*) from course")
    fun countCourse(): Int

    @Query("SELECT * FROM course WHERE id=:id")
    fun getCourseByID(id: Long): Course

    @Query("SELECT cover FROM course WHERE id=:id")
    fun getCover(id: Long): ByteArray

    @Query("select * FROM course WHERE courseTitle=:title")
    fun isIn(title: String): Course

    @Query("UPDATE course SET cover=:image  WHERE id=:id")
    fun updateCurrencyImage(id: Long, image: ByteArray): Int
}
