package company.com.stepikapi.model.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import company.com.stepikapi.model.dao.CourseDao
import company.com.stepikapi.model.entity.Course

@Database(entities = [Course::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract val courseDao: CourseDao

    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getAppDatabase(context: Context): AppDatabase {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "db_1"
                ).allowMainThreadQueries().build()
            }
            return INSTANCE ?: throw NullPointerException("Instance of appDataBase is null!")
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }

}